STKSZ	equ	16*1024
CODEP	equ	0
FLAGSP	equ	4
SPP	equ	8
MAX_INDEX	equ	99
MAX_ANGLE	equ 359
X_POS equ 0
Y_POS equ 4
ANGLE_POS equ 8
SCORE_POS equ 12

section .text
  align 16
    extern printf
    extern malloc
    extern free
    extern targeter
    extern printer
    extern sscanf
    extern drone
    extern scheduler
    extern getLoc
    extern droneData
    extern targeter
    global freeMem
    global x87_var
    global board            ;game board array of pointers each one is an array - probably
    global junk
;    global CORS
    global curNum
    global SPT
    global randomGen
    global	init_co_from_c
	global	start_co_from_c
	global	end_co
    global coRoutines
    global getLoc
    global CO1
    global CO2
    global CO3
    global main
    global mayDestroy
    global T
    global resume
    global N
    global K

section .data
    random: dw 0          
    normilizedRandom: dd 0
    N: dd 0
    T: dd 0
    K: dd 0
    beta: dd 0
    distance: dd 0
    coRoutines: dd 0
    stackPointers: dd 0
    junk: dd 0
    x87_var: dd 0
    ;seed: dw 0

;CORS:	dd	CO1
;	    dd	CO2
;	    dd	CO3

; Structures 
CO1:	dd	scheduler
Flags1:	dd	0
SP1:	dd	STK1+STKSZ

CO2:	dd	targeter
Flags2:	dd	0
SP2:	dd	STK2+STKSZ

CO3:	dd	printer
Flags3:	dd	0
SP3:	dd	STK3+STKSZ

section .rodata
    notEnoughCML: db "Error: Not enough command line arguments",10,0
    intFormat: db "%d",0
    wordFormat: db "%d",0;hd

section .bss
	align 16
        CURR:	resd	1
        SPT:	resd	1
        SPMAIN:	resd	1
        STK1:	resb	STKSZ
        STK2:	resb	STKSZ
        STK3:	resb	STKSZ


section .text
%macro startFunc 0                               
    pushad
    mov ebp, esp
%endmacro

%macro endFunc 0                               
    mov esp, ebp
    popad
%endmacro

%macro convertStr 3
    pushad
    push %1
    push %2
    push %3
    call sscanf
    pop dword[junk]
    pop dword[junk]
    pop dword[junk]
    popad

%endmacro

%macro normalize 1
    mov dword[x87_var],eax
    fild dword[x87_var]
    mov dword[x87_var],65535
    fidiv dword[x87_var]                          ;x/MAX_WORD value
    mov dword[x87_var],%1
    fimul dword[x87_var]                             ;%1 is max value            
    mov dword[x87_var],eax
    fstp dword[x87_var]
    mov eax,dword[x87_var]
%endmacro



%macro initM 2
    pushad
    push %1                     ;second param
    push %2                     ;first param
    call init_co_from_c
    pop dword[junk]
    pop dword[junk]
    popad
%endmacro
main:
    push ebp
    mov ebp, esp	
    ;pushad	
    
        ;extract command line arguments
    mov ecx,dword[esp+8]        ;take argc
    cmp ecx,7
    jne printError

    mov ecx,dword[esp+12]       ;take argv

    mov edx,dword[ecx+4]        ;ecx points to <N>
    convertStr N,intFormat,edx

    mov edx,dword[ecx+8]        ;ecx points to <T>
    convertStr T,intFormat,edx

    mov edx,dword[ecx+12]        ;ecx points to <K>
    convertStr K,intFormat,edx

    mov edx,dword[ecx+16]        ;ecx points to <beta>
    convertStr beta,intFormat,edx

    mov edx,dword[ecx+20]        ;ecx points to <distance>
    convertStr distance,intFormat,edx

    mov edx,dword[ecx+24]        ;ecx points to <seed>
    convertStr normilizedRandom,wordFormat,edx
    mov dx,word[normilizedRandom]
    mov word[random],dx


            ;convert angle to radians
    fldpi                               ;load pi to float
    fimul dword[beta]           ;mul pi with angle with 4byte float in ebx
    mov dword[x87_var],180
    fidiv dword[x87_var]                ;div result with 180 degrees 
        ;now in float stack there is angle in radians
    fstp dword[beta]
    
     ;allocate the array 
    mov eax ,dword[N]
    mov edx,4
    mul edx

    push eax
    call malloc
    pop dword[junk]
    mov dword[stackPointers],eax


        ;allocate the array 
    mov eax ,dword[N]
    mov edx,12
    mul edx

    push eax
    call malloc
    pop dword[junk]
    mov dword[coRoutines],eax

        ;allocate the drone_data array
    mov eax ,dword[N]
    mov edx,16
    mul edx

    push eax
    call malloc
    pop dword[junk]
    mov dword[droneData],eax

    p:
    call initAllRoutines

    push CO2
    call start_co_from_c
    pop dword[junk]


    jmp finishMain

    printError:
        push notEnoughCML
        call printf
        pop dword [junk]                            ;clean esp  

    finishMain:

    ;popad			
    mov esp, ebp	
    pop ebp
    ret


; Initalize a co-routine number given as an argument from C
init_co_from_c:
	push	EBP
	mov	EBP, ESP

	mov	ECX, [EBP+8]                            ;first param is index
    mov EAX,ECX
    mov edx,12
    mul edx
    mov EBX, [EBP+12]                           ;second param is pointer to struct
    add EBX,EAX
	;mov	EBX, [EAX+EDX]

	call	co_init

	pop	EBP
	ret
	
;  ECX holds the index of the co-routine
;  EBX is pointer to co-routine structure to initialize
co_init:
	pushad
	bts	dword [EBX+FLAGSP],0  ; test if already initialized
	jc	init_done
	mov	EAX,[EBX+CODEP] ; Get initial PC
	mov	[SPT], ESP
	mov	ESP,[EBX+SPP]   ; Get initial SP
	mov	EBP, ESP        ; Also use as EBP
    push    ECX             ;pass the index to the routine --todo check if there is a problem
	push	EAX             ; Push initial "return" address
	pushfd                   ; and flags
	pushad                   ; and all other regs
	mov	[EBX+SPP],ESP    ; Save new SP in structure
	mov	ESP, [SPT]      ; Restore original SP
init_done:
	popad
	ret

; EBX is pointer to co-init structure of co-routine to be resumed
; CURR holds a pointer to co-init structure of the curent co-routine
resume:
	pushfd			; Save state of caller
	pushad
	mov	EDX, [CURR]
	mov	[EDX+SPP],ESP	; Save current SP
do_resume:
	mov	ESP, [EBX+SPP]  ; Load SP for resumed co-routine
	mov	[CURR], EBX
	popad			; Restore resumed co-routine state
	popfd
	ret                     ; "return" to resumed co-routine!

; C-callable start of the first co-routine
start_co_from_c:
	push	EBP
	mov	EBP, ESP
	pushad
	mov	[SPMAIN], ESP             ; Save SP of main code

	mov	EBX, [EBP+8]		; Get number of co-routine
	;mov	EBX, [EBX*4+CORS]       ; and pointer to co-routine structure
	jmp	do_resume


; End co-routine mechanism, back to C main
end_co:
	mov	ESP, [SPMAIN]            ; Restore state of main code
	popad
	pop	EBP
	ret
    



randomGen:                      ;todo check if works -> loop and generator and print to check

    startFunc
    mov ax,word[random]
    mov bx,ax
    mov cx,ax

    ;take relevant bytes
    and ax, word 2              ;set eax to have only value of 14th leftmost bit
    and bx, word 4              ;set ebx to have only value of 13nd leftmost bit
    and cx, word 16             ;set ecx to have only value of 11nd leftmost bit

        ;move them to the same location -> LMB
    shl ax,14
    shl bx,13
    shl cx,11

        ;do xor operations
    xor ax,bx                   ;res in eax
    xor ax,cx                   ;res in eax

    shr word[random],1            ;shift the num to the right one time
    add word[random],ax           ;set LMB be as in eax

    
    endFunc
    mov eax,0
    mov ax, word[random]
    ret


initAllRoutines:
    startFunc

    mov ecx,0
    mov ebx,CO1
    call co_init
    mov ebx,CO2
    call co_init
    mov ebx,CO3
    call co_init
    ; initM CO1,0
    ; initM CO2,1
    ; initM CO3,2

    mov ecx,dword[N]
    againInit:
            push ecx                        ;safeguard ecx
            mov edx,ecx
            dec edx

            mov esi,dword[stackPointers]
            mov eax,edx
            push edx
            mov edx,4
            mul edx
            pop edx
            add esi,eax                     ;now esi pointes to the current drone in stackPointers

            mov ecx,dword[coRoutines]
            mov eax,edx
            push edx
            mov edx,12
            mul edx
            pop edx
            add ecx,eax
            mov dword[ecx+CODEP],drone      ;drone function
            mov dword[ecx+FLAGSP],0         ;reset flags to 0

            push esi
            push edx                        ;save guard edx
            push ecx                        ;save guard ecx
            push STKSZ
            call malloc
            pop dword[junk]
            pop ecx                         ;restore ecx -> current drone offset
            pop edx                         ;restore edx -> current drone index in array
            pop esi
            mov dword[esi],eax
            add eax,STKSZ                   ;move the pointer to the end of the stack, as stack is going down(otherwise we will overide unwanted space)
            mov dword[ecx+SPP],eax          ;set new stack for drone co-routine


            mov ecx,dword[droneData]        ;now ecx holds the droneData pointer
            mov eax,edx
            push edx
            mov edx,16
            mul edx
            pop edx
            add ecx,eax                     ;now ecx holds the pointer for the current drone structure


            call randomGen                   ;result is random number in eax
            normalize MAX_INDEX
            mov dword[x87_var],eax
            fld dword[x87_var]
            fstp dword[ecx+X_POS]         ;insert float x for drone

            call randomGen                   ;result is random number in eax
            normalize MAX_INDEX
            mov dword[x87_var],eax
            fld dword[x87_var]
            fstp dword[ecx+Y_POS]         ;insert float  y for drone

            call randomGen                   ;result is random number in eax
            normalize MAX_ANGLE
            mov dword[x87_var],eax
            fld dword[x87_var]
            fstp dword[ecx+ANGLE_POS]         ;insert float angle for drone

            mov dword[ecx+SCORE_POS],0       ;insert score for drone

            ;initM coRoutines,edx
            initM dword[coRoutines],edx
            pop ecx
            dec ecx
            cmp ecx, 0
            jne againInit
            ;loop againInit
    
    endFunc
    ret


    ;get position return true(1) if can destroy the target. false(0) else
mayDestroy:                                 
    push ebp
    mov ebp, esp

    ;startFunc
    mov ebx,dword[esp+8]                ;ebx is pointer to Drone Structure
    call getLoc                         ;eax hold pointer to structure for target x,y

    fld dword[eax+Y_POS]                ;load target y
    fsub dword[ebx+Y_POS]               ;subtract drone y

    fld dword[eax+X_POS]                ;load target x
    fsub dword[ebx+X_POS]               ;subtract drone x

    fpatan                              ;calc arctan[(y2-y1)/ (x2-x1)]=gamma in radians
    fst dword[x87_var]                             ;save gamma
    mov edx,dword[x87_var]

        ;convert angle to radians
    fldpi                               ;load pi to float
    fmul dword[ebx+ANGLE_POS]           ;mul pi with angle with 4byte float in ebx
    mov dword[x87_var],180
    fidiv dword[x87_var]                ;div result with 180 degrees 
        ;now in float stack there is angle in radians

    fsub
    ;fsub dword[ebx+ANGLE_POS]           ;gamma-alpha
    fabs                                ;|gamma-alpha|
    fldpi                               ;load pi to float = 180 in radians
    fcomip                              ;compare abs value to 180
    jbe fixLowerAngle

    fld dword[beta]
    fcomip                              ;compare abs to beta
    jb returnFalse                      ;todo: check jump sign
    jmp secondCondition

    fixLowerAngle:
        fstp dword[junk]                    ;clean x87 stack
            ;convert angle to radians
        fldpi                               ;load pi to float
        fmul dword[ebx+ANGLE_POS]           ;mul pi with angle with 4byte float in ebx
        mov dword[x87_var],180
        fidiv dword[x87_var]                ;div result with 180 degrees 
            ;now in float stack there is angle in radians

        mov dword[x87_var],edx              ;edx is gamma in radians
        fld dword[x87_var]
        fcomip                              ;compare gamma and alpha   todo:check sign
        jae  addToGamma
        fldpi
        mov dword[x87_var],2
        fmul dword[x87_var]                 ;2pi
        fadd                                ;alpha +2pi
        mov dword[x87_var],ebx
        ;fld dword[x87_var]                  ;load gamma
        fsub  dword[x87_var]                ;alpha-gamma

        fabs                                ;|gamma-alpha|
        fld dword[beta]
        fcomip                               ;todo check jump sign
        jb returnFalse                     
        jmp secondCondition

        addToGamma:
            mov dword[x87_var],ebx
            fld dword[x87_var]
            fldpi
            mov dword[x87_var],2
            fmul dword[x87_var]                 ;2pi
            fadd                                ;gamma +2pi
            fsub                            ;gamma-alpha
            fabs                            ;|gamma-alpha|
            fld dword[beta]
            fcomip                           ;todo check jump sign
            jl returnFalse
            jmp secondCondition

    secondCondition:
        fstp dword[junk]                    ;clean x87 stack
        fld dword[eax+Y_POS]                ;load target y
        fsub dword[ebx+Y_POS]               ;subtract drone y
        fst dword[x87_var]                  ;fld st(i) doesn't compile
        fld dword[x87_var]                  ;duplicate the angle change in radians
        fmul                                ;(y2-y1)^2

        fld dword[eax+X_POS]                ;load target x
        fsub dword[ebx+X_POS]               ;subtract drone x
        fst dword[x87_var]                  ;fld st(i) doesn't compile
        fld dword[x87_var]                  ;duplicate the angle change in radians
        fmul                                ;(x2-x1)^2

        fadd                                ;(y2-y1)^2+(x2-x1)^2
        fsqrt                               ;sqrt((y2-y1)^2+(x2-x1)^2)
        fild dword[distance]
        fcomip                           ;todo check jump sign
        jb returnFalse
        fstp dword[junk]                    ;clean x87 stack
    ;return true\

        ;free getLoc pair
    push eax
    call free
    pop dword[junk]

    returnTrue:
        mov eax,1
        mov esp, ebp	
        pop ebp
        ;endFunc
        ret

    returnFalse:
            ;free getLoc pair
        push eax
        call free
        pop dword[junk]

        fstp dword[junk]                    ;clean x87 stack
        mov eax,0
        mov esp, ebp	
        pop ebp
        ;endFunc
        ret

freeMem:
    startFunc
    mov esp,dword[SPMAIN]                   ;move to esp of main (so we can free the others)

    mov ecx,dword[N]
    againFree:
            push ecx                        ;safeguard ecx
            mov edx,ecx
            dec edx





            mov ecx,dword[stackPointers]
            mov eax,edx
            mov edx,4
            mul edx
            add ecx,eax


            mov eax,dword[ecx]          ;set new stack for drone co-routine
             

            ;change 
            ; mov edx,eax
            ; add edx,STKSZ
            ; loopReset:
            ;     mov byte[eax],0
            ;     inc eax
            ;     cmp eax,edx
            ;     jne loopReset

            ; ;finish change

    ;                                 pushad
    ;   push notEnoughCML
    ;         call printf
    ;         pop dword[junk]
    ;         popad

            ;pushad
            push dword[ecx]
            call free
            pop dword[junk] 
            ;popad

    ;         pushad
    ;   push notEnoughCML
    ;         call printf
    ;         pop dword[junk]
    ;         popad

            pop ecx
            dec ecx
            cmp ecx, 0
            jne againFree
            ;loop againInit
    


    push dword[droneData]
    call free
    pop dword[junk]

    push dword[coRoutines]
    call free
    pop dword[junk]

    push dword[stackPointers]
    call free
    pop dword[junk]

    mov     eax, 1              ; interrupt code
    mov     ebx, 0              ; argument, in this case: return value
    int     0x80

    endFunc
    ret