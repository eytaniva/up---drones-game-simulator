MAX_INDEX	equ	99
MAX_ANGLE	equ 359
MAX_ANGLE_CHANGE    equ 120
MAX_DISTANCE    equ 50
X_POS equ 0
Y_POS equ 4
ANGLE_POS equ 8
SCORE_POS equ 12

section .text
	align 16
	extern printf
    extern resume
    extern CO1
    extern T
    extern CO2
    extern freeMem
    extern coNum                   ;number of co-routine
    extern coRoutines
    extern exit
    extern mayDestroy
    extern x87_var
    extern randomGen
    extern junk
    extern T
    global droneData
    global createTarget
    global drone

section .data
    droneData: dd 0                 ;holds pointer to structures of size 16bit   x,y,angle,score each 4 bits          

section .rodata
    victoryText: db "Drone id <%d>: I am a winner",10,0


section .text


;ax should hold the random ,and %0 should be max value you want (range 0-max_value)
;answer is in eax
%macro normalize 1
    mov dword[x87_var],eax
    fild dword[x87_var]
    mov dword[x87_var],65535
    fidiv dword[x87_var]                          ;x/MAX_WORD value
    mov dword[x87_var],%1
    fimul dword[x87_var]                             ;%1 is max value            
    mov dword[x87_var],eax
    fstp dword[x87_var]
    mov eax,dword[x87_var]
%endmacro


;%1 lower bound , %2 upper bound and result will stay on the top of stack
%macro fixEdges 2
    push ecx
    push esi
    mov esi,0
    mov ecx, %2                         ;ecx holds the size of the cycle
    inc ecx             
    mov dword[x87_var],%1
    fild dword[x87_var]
    fcomip                             ;I part is for turning on flags of x86
    cmova esi,ecx                       ;set addition to angle change %2 degrees
        ;todo:check what happens when angle is above 360
    mov dword[x87_var],ecx
    neg ecx
    fild dword[x87_var]
    fcomip                              ;I part is for turning on flags of x86
    cmovb esi,ecx                      ;set addition to angle change -%2 degrees
    mov dword[x87_var],esi
    fiadd dword[x87_var]                ;apply angle change (in general case esi will be 0)
    pop esi
    pop ecx
%endmacro

drone:    
    pop ecx                             ;get index of drone
    push ecx                            
    mov eax,ecx
    mov edx,16                          ; size of droneData pos
    mul edx                             ;hope we dont go over max int
    mov ecx,eax                         ;now ecx is the offset in drone data for current drone
    add ecx,dword[droneData]            ;now ecx points to the start of the structure
    loopDrone:        
        call randomGen                  ;ax holds random num
        normalize MAX_ANGLE_CHANGE      ;generate a random number in range [0,120] degrees, with 16 b
        mov dword[x87_var],eax
        fld dword[x87_var]              ;load normalize angle
        mov dword[x87_var],60
        fisub dword[x87_var]            ;angle is between -60 to 60
        fstp dword[x87_var]             ;ebx holds the angle change
        mov ebx,dword[x87_var]          ;ebx is 4byte float
        

            ;angle change
        add ecx,ANGLE_POS               ;ecx is offset from droneData to angle of current drone
        fld dword[ecx]                  ;load old angle
        mov dword[x87_var],ebx          ;ebx is angle change
        fadd dword[x87_var]             ;change the angle
            ;edge cases when angle is greater 360 or lower than 0

        fixEdges 0,MAX_ANGLE             ;res in top of float stack
  
        fstp dword[ecx]                 ;extract new angle
        sub ecx,ANGLE_POS               ;ecx is offset from droneData to structure of current drone

            ;convert angle change to radians
        fldpi                           ;load pi to float
        mov dword[x87_var],ebx
        fmul dword[x87_var]             ;mul pi with angle change with 4byte float in ebx
        mov dword[x87_var],180
        fidiv dword[x87_var]            ;div result with 180 degrees 
            ;now in float stack there is angle change in radians
        
        fst dword[x87_var]              ;fld st(i) doesn't compile
        fld dword[x87_var]              ;duplicate the angle change in radians
        fsin                            ;calc delta y

        p:
        call randomGen                  ;ax holds random num
        normalize MAX_DISTANCE          ;generate a random number in range [0,50] degrees, with 16 b

        mov dword[x87_var],eax
        fmul dword[x87_var]            ;multiply by distance
        fstp dword[x87_var]
        mov esi,dword[x87_var]
        fcos                            ;calc delta x
        mov dword[x87_var],eax
        fmul dword[x87_var]            ;multiply by distance
        fstp dword[x87_var]
        mov edi,dword[x87_var]
            ;now float stack should be empty

        add ecx,X_POS
        fld dword[ecx]        ;load old x
        mov dword[x87_var],edi
        fadd dword[x87_var]
        fixEdges 0,MAX_INDEX
        fstp dword[ecx]       ;save new x
        sub ecx,X_POS                   ;reset ecx


        add ecx,Y_POS
        fld dword[ecx]        ;load old y
        mov dword[x87_var],esi
        fadd dword[x87_var]
        fixEdges 0,MAX_INDEX
        fstp dword[ecx]       ;save new y
        sub ecx,Y_POS                   ;reset ecx

        
        push ecx
        call mayDestroy                 ;res in eax. 1 if true
        pop ecx
        
        cmp eax,1                       ; if yes
        jne cantDestory
        ;can destroy
                ;target is destoryed then targeter is resumed
            add ecx,SCORE_POS
            inc dword[ecx]
            mov ebx,dword[T]
            cmp dword[ecx],ebx
            jge victory 
                ;not victory
            sub ecx,SCORE_POS

            mov ebx,CO2                 ;resume target (C02 address for Scheduler structure)
            call resume
            

        jmp continue
        cantDestory:
            mov ebx,CO1                 ;resume scheduler (C01 address for Scheduler structure)
            call resume

        continue:
        jmp loopDrone

        victory:
            pop ecx                     ;index of drone
            push ecx
            push victoryText
            call printf
            pop dword[junk]             
            pop dword[junk]    

            call freeMem

            
