X_POS equ 0
Y_POS equ 4
ANGLE_POS equ 8
SCORE_POS equ 12

section .rodata
    targetFormat:db "%.2f,%.2f",10,0        ; db "%d,%d",10,0  
    droneFormat: db "%d,%.2f,%.2f,%.2f,%d",10,0
    lineStr: db "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",10,0

section .text
	align 16
	extern printf
    extern resume
    global printer
    extern free
    extern CO1                          ;scheduler structure
    extern N                            ;number of co-routine
    extern getLoc
    extern junk
    extern droneData


printer:
    call getLoc
    push eax                           ;for free after printf
        ;print x,y


    sub esp,16                          ;reserve stack for 2 doubles in stack
    add eax,4                           ;get y address (offset from structure)
    fld dword[eax]   
    fstp qword[esp]                         ;push y into stack
    sub eax,4                           ;get x address (offset from structure)
    fld dword[eax]
    fstp qword[esp+8]                            ;push y

    ;    push lineStr                        ;debug

    push targetFormat        
    call printf       
    add esp,20
    ;    pop dword[junk]                     ;debug XXXX clean


        ;free memory
    
    call free
    pop dword[junk]

    mov ecx, dword[N]                   ;number of drones
    loopPrinter:
        push ecx                        ;save guard ecx
        mov ebx,dword[N]
        mov esi, dword[droneData]
        sub ebx,ecx                     ;ebx hold index in droneData
        mov eax,ebx
        mov edx, 16
        mul edx                          ;eax hold offset of current drone in droneData

        
        
        add eax,SCORE_POS               ;get score offset

        push dword[esi+eax]             ;push drone score   
        sub eax,SCORE_POS               ;revert

        sub esp,24                          ;reserve stack for 3 doubles in stack
        add eax,ANGLE_POS               ;get angle offset       
        ;push dword[esi+eax]            ;push drone angle 
        fld dword[esi+eax]   
        fstp qword[esp+16]                 ;push drone angle  
        sub eax,ANGLE_POS               ;revert

        add eax,Y_POS                   ;get y offset       
        ;push dword[esi+eax]             ;push drone y  
        fld dword[esi+eax]   
        fstp qword[esp+8]                 ;push drone y    
        sub eax,Y_POS                   ;revert

        add eax,X_POS                   ;get x offset       
        ;push dword[droneData+eax]       ;push drone x
        fld dword[esi+eax]   
        fstp qword[esp]                 ;push drone x    
        sub eax,X_POS                   ;revert

        inc ebx                         ; index for print starts from 1 and not 0
        push ebx                        ;push index of droneData
        push droneFormat
        call printf
        add esp,24                      ;clean doables
        pop dword[junk]                 ;clean droneFormat
        pop dword[junk]                 ;clean score
        pop dword[junk]
        ; pop dword[junk]
        ; pop dword[junk]
        ; pop dword[junk]

        pop ecx                         ;restore ecx
        loop loopPrinter

    mov ebx,CO1                     ;call resume with scheduler
    call resume
    jmp printer
    







	