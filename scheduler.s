
section .text
	align 16
	extern printf
    extern resume
    global scheduler
    extern CO3
    extern N                   ;number of co-routine
    extern SPT
    extern K
    extern coRoutines

scheduler:
    mov edx,dword[K]
    schedulerStart:
        mov ecx,dword[N]                        ;take number of co-routine for loop
        mov eax,dword[coRoutines]                         ;start of stracture array

        again:
                ;mov ebx,dword [eax]         
            mov ebx,eax                            ;ebx holds the start of the current co-routine stracture
            call resume
            add eax,12                              ;mov to next co-routine stracture
            dec edx
            cmp edx,0
            je printBoard
            loop again

            jmp schedulerStart                      ;round robin

            printBoard:
                mov edx,dword[K]
                mov ebx,CO3
                call resume
                loop again
                jmp schedulerStart                      ;round robin

            ;mov esp,[SPT]
            ;sub esp,4                   ;? is needed?
            ;ret






	