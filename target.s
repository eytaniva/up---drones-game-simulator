MAX_INDEX	equ	99

section .data
    x: dd 0
    y: dd 0
    pointerHolder: dd 0     ;pair<x,y>, while x is in [pointerHolder] and y is in [pointerHolder+1]


section .text
  align 16
    extern printf
    extern malloc 
    extern CO1              ;scheduler structure
    extern resume
    extern randomGen
    extern board            ;game board array of pointers each one is an array - probably
    extern junk             ;clean stuck
    global createTarget
    global getLoc
    global targeter
    extern x87_var

%macro startFunc 0                               
    pushad
    mov ebp, esp
%endmacro

%macro endFunc 0                               
    mov esp, ebp
    popad
%endmacro

%macro createLink 0         ;res in eax
    push dword 8
    call malloc
    pop dword[junk]
%endmacro



;ax should hold the random ,and %0 should be max value you want (range 0-max_value)
;answer is in eax
%macro normalize 1
    mov dword[x87_var],eax
    fild dword[x87_var]
    mov dword[x87_var],65535
    fidiv dword[x87_var]                          ;x/MAX_WORD value
    mov dword[x87_var],%1
    fimul dword[x87_var]                             ;%1 is max value            
    mov dword[x87_var],eax
    fstp dword[x87_var]
    mov eax,dword[x87_var]
%endmacro


targeter:
    ;startFunc

    call createTarget                       ;create new x,y values for target and returns a pointer to a pair
    
    mov ebx,CO1                             ;pass Sheduler structure to resume
    call resume

    jmp targeter
    ;endFunc
    ret


createTarget:
    startFunc
    ;createLink
    ;mov dword[pointerHolder],eax            ;new link address created is stored in pointerHolder
    call randomGen                          ;al store the result
    normalize MAX_INDEX


    mov dword[x],eax                        ;store for later
    ;mov ebx,dword[pointerHolder]            ;extract the address
    ;mov dword[ebx],eax

    call randomGen                          ;al store the result
    normalize MAX_INDEX

    mov dword[y],eax                        ;store for later
    ;mov ebx,dword[pointerHolder]            ;extract the address
    ;mov dword[ebx+4],eax

    endFunc
    ;mov eax,dword[pointerHolder]            ;move result into eax
    ret


getLoc:
    startFunc
    createLink
    
    mov dword[pointerHolder],eax            ;new link address created is stored in pointerHolder
    mov ebx,dword[x]                          ;store for later
    mov dword[eax],ebx

    mov ebx,dword[y]                          ;store for later
    mov dword[eax+4],ebx

    endFunc
    mov eax,dword[pointerHolder]            ;move result into eax
    ret    